import { Component, OnInit} from '@angular/core';

//Importamos nuestro model que esta un nivel arriba en models
import { DestinoViaje } from './../models/destino-viaje.model';


@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {

  //destinos : string[]; MOdulo 1 leccion 2
  destinos: DestinoViaje[];

  constructor() { 
  	this.destinos = [];
  	//this.destinos = ['Madrid','Barcelona','Asturias','Murcia','Valencia']; Modulo 1 leccion2 
  }

  ngOnInit(): void {
  }

  
  //El metodo guardar, que guarda en la clase DestinoViaje el nombre y la url
  guardar(d: DestinoViaje) {

  	//Guardo el destino Elegido
    this.destinos.push(d);
    
    //mostramos por consola
    console.log(this.destinos);
    
  	
  }

  //Metodo por el cual si elegimos esa card mostramos como Preferida esa y deseleccionamos las demas
  elegido(d : DestinoViaje){

    
    //Recorremos los destinos y por cada uno de ellos ponemos que no esta seleccionado llamando a setSelected de ese DestinoViaje
    this.destinos.forEach(function (x) {x.setSelected(false)});

    //el DestinoViaje seleccionado lo ponemos a true;
    d.setSelected(true);
      
  }

}
