export class DestinoViaje {
	
	//Si no indicamos nada , selected se precarga a false
	private selected : boolean = false;

	public servicios : string[];

	//Cremos el contructor declarando las variables publicas , es como si las crearamos arriba y se le añade lo que pasamos
	constructor(public nombre: string, public u:string) { 
		this.servicios = ['niños gratis','desayuno'];
	}

	//Metodo si es seleccionado
	isSelected(): boolean{
		return this.selected;
	}

	//Metodo Set Selected
	setSelected(isSelected : boolean) {
		this.selected = isSelected;
	}

}