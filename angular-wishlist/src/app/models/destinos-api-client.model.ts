import { DestinoViaje } from './destino-viaje.model';

export class DestinosApiClient {

	//Array de destinos viajes
	destinos:DestinoViaje[];
	
	//Inicializamos el array
	constructor() {
       this.destinos = [];
	}

	//Metodo para añadir un destinoViaje al array
	add(d:DestinoViaje){
	  this.destinos.push(d);
	
	}

	//Metodo que devuelve el array
	getAll(){
	  return this.destinos;
    }
} 