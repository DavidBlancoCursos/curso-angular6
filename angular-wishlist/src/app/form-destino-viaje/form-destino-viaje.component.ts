import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';

//Importamos los componentes para los formularios
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})

export class FormDestinoViajeComponent implements OnInit {

  //El invocador para llamar al padre pasandole un destinoViaje
  @Output() onItemAdded: EventEmitter<DestinoViaje>;

  //Variable para controlar el formulario
  fg: FormGroup;

  //Inicializamos el constructor con el invocador y el formulario con los parametros de este.
  constructor(fb: FormBuilder) { 
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: [''],
      url: ['']
    });
  }

  ngOnInit(): void {
  }

  guardar(nombre: string, url:string): boolean {

  	//Creamos el destino viaje
  	let d = new DestinoViaje(nombre, url);
    
  //mostramos por consola
    console.log("guardamos la seleccion y lo pasamos a Lista-destinos en onItemAdded");
    console.log(d);

    //Lo enviamos al padre 
    this.onItemAdded.emit(d);

    
    
  	//retornamos false para que no se recargue la pagina ya que se invoca desde un boton
  	return false
  } 

}
