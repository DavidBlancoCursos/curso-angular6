/* Introducimos el Input porque le vamos a pasar de lista destinos el valor de una variable y HostBinding
  para poder meter en el componente que genera angular en html el estilo para que no nos lo cargue uno debajo de otro
  y si que se use el col-md-4 de bootstrap y podamos meter 3 componentes en cada fila hasta ocupar los 12 que nos da
*/ 
import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';

//Importamos nuestro model que esta un nivel arriba en models
import { DestinoViaje } from './../models/destino-viaje.model';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})

export class DestinoViajeComponent implements OnInit {

  /*nombre puede ser pasado como parametro si usamos en el invocador el uso de
  	*ngFor="let x of destinos" [nombre]="x" donde destinos es el array con los valores a pasar
   Modulo 1 leccion 2 */

  /* Ahora es destino lo que estamos recibiendo es la clase DestinoViaje en el *ngFor del html de Lista-destinos
  *ngFor="let d of destinos" [destino]="d" */
  @Input() destino: DestinoViaje;

  //usamos el alias idx desde el html y guardamos en position la posicion del array
  @Input("idx") position: number;

  /*Con esta directiva lo que hacemos es ponerle al encapsulado que genera angular la clase, en nuestro caso
    col-md-4 que es la que nos destruia el componente y la quitamos del html
  */
  @HostBinding('attr.class') cssClass = 'col-md-4';

  //Le he añadido al componente al padding 10px para que se separen de la parte de arriba las tarjetas y se vea mejor
  @HostBinding('style.padding-top') padding_top = '10px';

  //Creamos una variable invocadora de evento de tipo DestinoViaje para poder pasar al listado un DestinoViaje concreto
  @Output() clicked: EventEmitter<DestinoViaje>;

  //Inicializamos el invocador de evento.
  constructor() {
    this.clicked = new EventEmitter();
    
  }

  ngOnInit(): void {
  }

   /*El metodo ir lanza la invocacion del clicked pasandole el DestinoViaje clickeado al padre que contiene 
   el resto de destinos viajes por si hay alguno marcado como preferido quitarlo
   */
  ir(): boolean {

    //Pasamos el DestinoViaje seleccionado al padre que este llamara a la funcion para desmarcar a los otros y marcar este, 
    
    //Invocamos el evento clicked del padre , en nuestro caso llamara a la funcion elegido(DestinoViaje)
    this.clicked.emit(this.destino);

    console.log("metodo ir destino-viaje");
    return false;
  }

}